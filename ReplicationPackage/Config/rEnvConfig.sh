#!/usr/bin/env bash

#config the R_HOME variable according to your R installation
R_HOME=/Library/Frameworks/R.framework/Resources

#config the R_JAVA variable according to your R_JAVA installation
R_JAVA=${HOME}/Library/R/3.3/library/rJava/jri/

export R_HOME