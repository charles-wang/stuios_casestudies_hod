package config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chunhui.wang on 23/02/2017.
 */
public class ScenarioToSig {
    private static Map<String, String> sigMap = new HashMap<>();

    static {
        sigMap.put("s[1]","DETECT|UNDER_VOLTAGE_ERROR");
        sigMap.put("s[2]","UNDETECT|UNDER_VOLTAGE_ERROR");
        sigMap.put("s[3]","DETECT|UNDER_VOLTAGE_ERROR");
        sigMap.put("s[4]","UNDETECT|UNDER_VOLTAGE_ERROR");
        sigMap.put("s[5]","DETECT|OVER_VOLTAGE_ERROR");
        sigMap.put("s[6]","UNDETECT|OVER_VOLTAGE_ERROR");
        sigMap.put("s[7]","DETECT|OVER_VOLTAGE_ERROR");
        sigMap.put("s[8]","UNDETECT|OVER_VOLTAGE_ERROR");
        sigMap.put("s[9]","DETECT|VBAT_OUT_OF_RANGE_ERROR");
        sigMap.put("s[10]","UNDETECT|VBAT_OUT_OF_RANGE_ERROR");
        sigMap.put("s[11]","DETECT|VBAT_OUT_OF_RANGE_ERROR");
        sigMap.put("s[12]","UNDETECT|VBAT_OUT_OF_RANGE_ERROR");
        sigMap.put("s[13]","DETECT|NVM_ACCESS_ERROR");
        sigMap.put("s[14]","DETECT|NVM_CRC_ERROR");
        sigMap.put("s[15]","DETECT|ALGO_NVM_CRC_ERROR");
        sigMap.put("s[16]","DETECT|ALGO_SELECTION_ERROR");
        sigMap.put("s[17]","DETECT|CALIBRATION_DATA_ERROR");
        sigMap.put("s[18]","DETECT|ROM_ERROR");
        sigMap.put("s[19]","DETECT|RAM_ERROR");
        sigMap.put("s[20]","DETECT|ALU_ERROR");
        sigMap.put("s[21]","DETECT|WATCHDOG_RESET_ERROR");
        sigMap.put("s[22]","DETECT|SPM_ERROR");
        sigMap.put("s[23]","DETECT|ASIC_AK_PARITY_ERROR");
        sigMap.put("s[24]","UNDETECT|ASIC_AK_PARITY_ERROR");
        sigMap.put("s[25]","DETECT|ASIC_AK_PARITY_ERROR");
        sigMap.put("s[26]","UNDETECT|ASIC_AK_PARITY_ERROR");
        sigMap.put("s[27]","DETECT|ASIC_COM_ERROR");
        sigMap.put("s[28]","UNDETECT|ASIC_COM_ERROR");
        sigMap.put("s[29]","DETECT|ASIC_COM_ERROR");
        sigMap.put("s[30]","UNDETECT|ASIC_COM_ERROR");
        sigMap.put("s[31]","DETECT|CAPA_OFFSET_REAL_TOO_LOW_ERROR");
        sigMap.put("s[32]","UNDETECT|CAPA_OFFSET_REAL_TOO_LOW_ERROR");
        sigMap.put("s[33]","DETECT|CAPA_OFFSET_REAL_TOO_LOW_ERROR");
        sigMap.put("s[34]","UNDETECT|CAPA_OFFSET_REAL_TOO_LOW_ERROR");
        sigMap.put("s[35]","DETECT|CAPA_OFFSET_REAL_TOO_HIGH_ERROR");
        sigMap.put("s[36]","UNDETECT|CAPA_OFFSET_REAL_TOO_HIGH_ERROR");
        sigMap.put("s[37]","DETECT|CAPA_OFFSET_REAL_TOO_HIGH_ERROR");
        sigMap.put("s[38]","UNDETECT|CAPA_OFFSET_REAL_TOO_HIGH_ERROR");
        sigMap.put("s[39]","DETECT|CAPA_OFFSET_IMAG_TOO_LOW_ERROR");
        sigMap.put("s[40]","UNDETECT|CAPA_OFFSET_IMAG_TOO_LOW_ERROR");
        sigMap.put("s[41]","DETECT|CAPA_OFFSET_IMAG_TOO_LOW_ERROR");
        sigMap.put("s[42]","UNDETECT|CAPA_OFFSET_IMAG_TOO_LOW_ERROR");
        sigMap.put("s[43]","DETECT|CAPA_OFFSET_IMAG_TOO_HIGH_ERROR");
        sigMap.put("s[44]","UNDETECT|CAPA_OFFSET_IMAG_TOO_HIGH_ERROR");
        sigMap.put("s[45]","DETECT|CAPA_OFFSET_IMAG_TOO_HIGH_ERROR");
        sigMap.put("s[46]","UNDETECT|CAPA_OFFSET_IMAG_TOO_HIGH_ERROR");
        sigMap.put("s[47]","DETECT|CAPA_LD_CURR_TOO_HIGH_ERROR");
        sigMap.put("s[48]","UNDETECT|CAPA_LD_CURR_TOO_HIGH_ERROR");
        sigMap.put("s[49]","DETECT|CAPA_LD_CURR_TOO_HIGH_ERROR");
        sigMap.put("s[50]","UNDETECT|CAPA_LD_CURR_TOO_HIGH_ERROR");
        sigMap.put("s[51]","DETECT|ALGO_EXEC_ERROR");
        sigMap.put("s[52]","UNDETECT|ALGO_EXEC_ERROR");
        sigMap.put("s[53]","DETECT|ALGO_EXEC_ERROR");
        sigMap.put("s[54]","UNDETECT|ALGO_EXEC_ERROR");
        sigMap.put("s[55]","DETECT|ALGO_REAL_LOW_CUTOFF_ERROR");
        sigMap.put("s[56]","UNDETECT|ALGO_REAL_LOW_CUTOFF_ERROR");
        sigMap.put("s[57]","DETECT|ALGO_REAL_LOW_CUTOFF_ERROR");
        sigMap.put("s[58]","UNDETECT|ALGO_REAL_LOW_CUTOFF_ERROR");
        sigMap.put("s[59]","DETECT|ALGO_REAL_HIGH_CUTOFF_ERROR");
        sigMap.put("s[60]","UNDETECT|ALGO_REAL_HIGH_CUTOFF_ERROR");
        sigMap.put("s[61]","DETECT|ALGO_REAL_HIGH_CUTOFF_ERROR");
        sigMap.put("s[62]","UNDETECT|ALGO_REAL_HIGH_CUTOFF_ERROR");
        sigMap.put("s[63]","DETECT|ALGO_IMAG_LOW_CUTOFF_ERROR");
        sigMap.put("s[64]","UNDETECT|ALGO_IMAG_LOW_CUTOFF_ERROR");
        sigMap.put("s[65]","DETECT|ALGO_IMAG_LOW_CUTOFF_ERROR");
        sigMap.put("s[66]","UNDETECT|ALGO_IMAG_LOW_CUTOFF_ERROR");
        sigMap.put("s[67]","DETECT|ALGO_IMAG_HIGH_CUTOFF_ERROR");
        sigMap.put("s[68]","UNDETECT|ALGO_IMAG_HIGH_CUTOFF_ERROR");
        sigMap.put("s[69]","DETECT|ALGO_IMAG_HIGH_CUTOFF_ERROR");
        sigMap.put("s[70]","UNDETECT|ALGO_IMAG_HIGH_CUTOFF_ERROR");
        sigMap.put("s[71]","DETECT|ASIC_RESET_ERROR");
        sigMap.put("s[72]","UNDETECT|ASIC_RESET_ERROR");
        sigMap.put("s[73]","DETECT|ASIC_RESET_ERROR");
        sigMap.put("s[74]","UNDETECT|ASIC_RESET_ERROR");
        sigMap.put("s[75]","DETECT|ASIC_COMP_TEST_C404142_VCH_TOO_LOW_ERROR");
        sigMap.put("s[76]","UNDETECT|ASIC_COMP_TEST_C404142_VCH_TOO_LOW_ERROR");
        sigMap.put("s[77]","DETECT|ASIC_COMP_TEST_C404142_VCH_TOO_LOW_ERROR");
        sigMap.put("s[78]","UNDETECT|ASIC_COMP_TEST_C404142_VCH_TOO_LOW_ERROR");
        sigMap.put("s[79]","DETECT|ASIC_COMP_TEST_C404142_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[80]","UNDETECT|ASIC_COMP_TEST_C404142_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[81]","DETECT|ASIC_COMP_TEST_C404142_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[82]","UNDETECT|ASIC_COMP_TEST_C404142_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[83]","DETECT|ASIC_COMP_TEST_CREF_VCH_TOO_LOW_ERROR");
        sigMap.put("s[84]","UNDETECT|ASIC_COMP_TEST_CREF_VCH_TOO_LOW_ERROR");
        sigMap.put("s[85]","DETECT|ASIC_COMP_TEST_CREF_VCH_TOO_LOW_ERROR");
        sigMap.put("s[86]","UNDETECT|ASIC_COMP_TEST_CREF_VCH_TOO_LOW_ERROR");
        sigMap.put("s[87]","DETECT|ASIC_COMP_TEST_CREF_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[88]","UNDETECT|ASIC_COMP_TEST_CREF_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[89]","DETECT|ASIC_COMP_TEST_CREF_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[90]","UNDETECT|ASIC_COMP_TEST_CREF_VCH_TOO_HIGH_ERROR");
        sigMap.put("s[91]","DETECT|ASIC_COMP_TEST_C404142_ELECTRICAL_DEVICE_ERROR");
        sigMap.put("s[92]","UNDETECT|ASIC_COMP_TEST_C404142_ELECTRICAL_DEVICE_ERROR");
        sigMap.put("s[93]","DETECT|ASIC_COMP_TEST_C404142_ELECTRICAL_DEVICE_ERROR");
        sigMap.put("s[94]","UNDETECT|ASIC_COMP_TEST_C404142_ELECTRICAL_DEVICE_ERROR");
        sigMap.put("s[95]","DETECT|GUARD_DIAG_ERROR");
        sigMap.put("s[96]","UNDETECT|GUARD_DIAG_ERROR");
        sigMap.put("s[97]","DETECT|GUARD_DIAG_ERROR");
        sigMap.put("s[98]","UNDETECT|GUARD_DIAG_ERROR");
        sigMap.put("s[99]","DETECT|CALIBRATION_NOT_LEARNED_ERROR");
        sigMap.put("s[100]","UNDETECT|CALIBRATION_NOT_LEARNED_ERROR");
        sigMap.put("s[101]","DETECT|CALIBRATION_NOT_LEARNED_ERROR");
        sigMap.put("s[102]","UNDETECT|CALIBRATION_NOT_LEARNED_ERROR");
    }

    public static String getSig(String scenario) {
        return sigMap.get(scenario);
    }
}
