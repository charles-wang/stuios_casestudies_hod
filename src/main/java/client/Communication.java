package client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by chunhui.wang on 23/02/2017.
 */
public class Communication {
    final static Logger LOGGER = LoggerFactory.getLogger(Communication.class);
    private final String serverHostname = "127.0.0.1";
    private final int defaultPort = 10008;

    private int serverPort;
    private Socket echoSocket = null;
    private PrintWriter out = null;
    private BufferedReader in = null;



    public Communication() {
        String port = System.getProperty("Port");
        try {
            serverPort = Integer.parseInt(port);
        } catch (Exception e) {
            serverPort = defaultPort;
        }
        try {
            echoSocket = new Socket(serverHostname, serverPort);
            LOGGER.info("echoSocket is connected : {}. Port: {}", echoSocket.isConnected(), echoSocket.getLocalPort());
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            LOGGER.info("Output Stream created!");
            in = new BufferedReader(new InputStreamReader(
                    echoSocket.getInputStream()));
            LOGGER.info("input Stream created!");
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            LOGGER.info("Couldn't get I/O for "
                    + "the connection to: {}", serverHostname);
            System.exit(1);
        }
    }

    public boolean sendMessage(String msg) {
        try {
            LOGGER.info("SEND MSG: {}", msg);
            //continue send msg if the msg is not yet received
            //do{
                //LOGGER.info("SEND MSG: {}", msg);
                out.println(msg);
                //Thread.sleep(100);
            //} while(!in.ready());

            return in.readLine().equals("ack:" + msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return false;
    }

    public String sendRequest(String request) {
        out.println(request);
        String response = null;
        try {
            response = in.readLine();
            LOGGER.info("res:{}", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public void close() {
        try {
            out.flush();
            out.close();
            in.close();
            echoSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
