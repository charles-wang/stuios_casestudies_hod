package testadaptor;

import com.google.gson.JsonObject;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import client.TestDriverClient;
import hytest.HyTest;
import hytest.HyTester;

import static org.junit.Assert.assertTrue;

/**
 * Created by chunhui.wang on 6/11/2016.
 */
public abstract class BodySenseAdaptor extends ConcreteTestAdaptor {
    final static Logger LOGGER = LoggerFactory.getLogger(BodySenseAdaptor.class);

    public abstract void generatedTest();

    public double getProbability() { return 1.0; };

    @Override
    public void executeScenario(String arg) {
        parseTestStep(ScenarioStep.class, arg);
    }

    @Override
    public void sendMessage(String arg) {
        parseTestStep(MessageStep.class, arg);
    }

    @Override
    public void verify(String arg) {
        parseTestStep(VerifyStep.class, arg);
    }

    @Override
    public List<String> generateTestSteps() {
        generatedTest();
        List<String> result = new ArrayList<>();
        List<TestStep> optimizedTestSteps = optimizeTestSteps(testSteps);
        result.addAll(optimizedTestSteps.stream().map(TestStep::toTestScript).collect(Collectors.toList()));
        return result;
    }

    private void parseTestStep(Class classObj, String arg) {
        JsonObject jsonObject = parseJson(arg);
        int timestamp = getTimestamp(jsonObject);
        //timestamp = (classObj == ScenarioStep.class ? timestamp - 1700 : timestamp);
        generateWaitTime(timestamp);
        recordTestStep(TestStepFactory.createTestStep(classObj, jsonObject));
    }

    @Override
    protected void recordTestStep(TestStep step) {
        testSteps.add(step);
    }

    private List<TestStep> optimizeTestSteps(List<TestStep> testSteps) {
        ScenarioStep lastScenario = null;
        TestStep lastTestStep = null;
        List<TestStep> result = new ArrayList<>();
        for(TestStep step : testSteps) {
            if(lastTestStep != null && lastTestStep.canAdjacentMerge(step)) {
                lastTestStep.merge(step);
                continue;
            } else if(step instanceof ScenarioStep) {
                if (!step.canAdjacentMerge(lastScenario)) {
                    result.add(step);
                    lastScenario = (ScenarioStep) step;
                    lastTestStep = step;
                }
                continue;
            }
            result.add(step);
            lastTestStep = step;
        }
        return result;
    }

    protected TestDriverClient client = null;
    protected HyTester hyTester = null;

    @Before
    public void before() throws Exception {
        client = new TestDriverClient();
        hyTester = new HyTester();
    }

    @Test
    public void execute() throws Exception {
        double probability = getProbability();
        List<String> tests = generateTestSteps();
        boolean result = false;
        if(probability == 1.0)
        {
            result = client.executeTestCase(tests);
        } else {
            int total = 0;
            int positive = 0;
            while(true) {
                positive += client.executeTestCase(tests) ? 1 : 0;
                total++;
                LOGGER.info("In total : {} Runs. {} pass, {} failed.", total, positive, total - positive);
                HyTest hyRes = hyTester.hyTest(total, positive, probability);
                LOGGER.info("HyTest Result: {}", hyRes);
                if(hyRes == HyTest.ACCEPT) {
                    result = true;
                    break;
                }

                if(hyRes == HyTest.REJECT) {
                    result = false;
                    break;
                }
                client.resetCommunication();
            }
        }
        hyTester.end();
        assertTrue(result);
    }
}
