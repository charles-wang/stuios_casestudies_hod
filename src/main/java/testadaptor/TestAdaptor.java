package testadaptor;

import java.util.List;

/**
 * Created by chunhui.wang on 6/11/2016.
 */
public interface TestAdaptor {
    void executeScenario(String arg);

    void sendMessage(String arg);

    void verify(String arg);

    List<String> generateTestSteps();

}
