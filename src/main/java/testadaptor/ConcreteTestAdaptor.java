package testadaptor;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chunhui.wang on 6/12/2016.
 */
public abstract class ConcreteTestAdaptor implements TestAdaptor {
    JsonParser parser;
    List<TestStep> testSteps;
    int preTime;

    public ConcreteTestAdaptor() {
        this.parser = new JsonParser();
        testSteps = new ArrayList<>();
        preTime = 0;
    }

    private int getPreTime() {
        return preTime;
    }

    private void setPreTime(int preTime) {
        this.preTime = preTime;
    }

    public JsonObject parseJson(String json) {
        return (JsonObject) parser.parse(json);
    }

    public int getTimestamp(JsonObject o) {
        return o.has("timestamp") ? o.get("timestamp").getAsInt() : -1;
    }

    public void generateWaitTime(int timestamp) {
        int delta = timestamp - getPreTime();
        setPreTime(timestamp);
        if(delta > 0)
            recordTestStep(new WaitStep(delta));
    }

    protected abstract void recordTestStep(TestStep step);

}
