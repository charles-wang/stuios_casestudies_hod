package testadaptor;

/**
 * Created by chunhui.wang on 6/13/2016.
 */
public class WaitStep implements TestStep {
    private int time;
    private final int offset = 10;

    public WaitStep(int time) {
        this.time = time + offset;
    }

    public int getTime() {
        return time;
    }

    @Override
    public String toTestScript() {
        return "WAIT:" + time;
    }

    @Override
    public void merge(Object o) {
        this.time += ((WaitStep) o).getTime();
    }

    @Override
    public boolean canAdjacentMerge(Object o) {
        return o != null && o instanceof WaitStep;
    }
}
