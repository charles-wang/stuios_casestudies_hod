package testadaptor;

import com.google.gson.JsonObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by chunhui.wang on 6/13/2016.
 */
public class TestStepFactory {
    public static TestStep createTestStep(Class classobj, JsonObject object) {
        Class<?> classObject = null;
        TestStep result = null;
        try {
            classObject = Class.forName(classobj.getName());
            Constructor<?>[] cons = classObject.getConstructors();
            result = (TestStep) cons[0].newInstance(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;

    }
}
