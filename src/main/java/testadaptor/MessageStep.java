package testadaptor;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chunhui.wang on 6/13/2016.
 */
public class MessageStep implements TestStep{

    private String messageName;
    private Map<String, String> parameters;

    public MessageStep(JsonObject object) {
        messageName = object.get("message").getAsString();
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        parameters = gson.fromJson(object.get("parameters"), type);

    }

    @Override
    public String toTestScript() {
        return "MSG:"+ messageName + "(" + parameters.entrySet().toString().replaceAll("[\\[|\\]]","") + ")";
        //return "sendMessage(\"" + messageName + "," + parameters.entrySet().toString() + "\");";
    }

    @Override
    public void merge(Object o) {

    }

    @Override
    public boolean canAdjacentMerge(Object o) {
        return false;
    }
}
