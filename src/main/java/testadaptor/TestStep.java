package testadaptor;


/**
 * Created by chunhui.wang on 6/13/2016.
 */
public interface TestStep {

    String toTestScript();

    void merge(Object o);

    boolean canAdjacentMerge(Object o);
}
