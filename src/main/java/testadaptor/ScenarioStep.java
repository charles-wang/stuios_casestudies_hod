package testadaptor;

import com.google.gson.JsonObject;

import config.ScenarioToSig;

/**
 * Created by chunhui.wang on 6/13/2016.
 */
public class ScenarioStep implements TestStep{
    private String scenarioName;

    public ScenarioStep(JsonObject object) {
        scenarioName = object.get("scenario").getAsString();
    }

    public String getScenarioName() {
        return scenarioName;
    }

    @Override
    public String toTestScript() {
        String prefix = "MSG:";
        return prefix + ScenarioToSig.getSig(scenarioName);
    }

    @Override
    public void merge(Object o) {

    }

    @Override
    public boolean canAdjacentMerge(Object o) {
        return o != null && o instanceof ScenarioStep && ((ScenarioStep)o).getScenarioName().equals(scenarioName);
    }


}
