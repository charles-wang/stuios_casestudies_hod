package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author chunhui.wang 04/08/2017
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        tauc.ts0.TC000.class,
        tauc.ts0.TC001.class,
        tauc.ts0.TC002.class,
        tauc.ts0.TC003.class,
        tauc.ts0.TC004.class,
        tauc.ts0.TC005.class,
        tauc.ts0.TC006.class,
        tauc.ts0.TC007.class,
        tauc.ts0.TC008.class,
        tauc.ts0.TC009.class,
        tauc.ts0.TC010.class,
        tauc.ts0.TC011.class,
        tauc.ts0.TC012.class,
        tauc.ts0.TC013.class,
        tauc.ts0.TC014.class,
        tauc.ts0.TC015.class,
        tauc.ts0.TC016.class,
        tauc.ts0.TC017.class,
        tauc.ts0.TC018.class,
        tauc.ts0.TC019.class,
        tauc.ts0.TC020.class,
        tauc.ts0.TC021.class,
        tauc.ts0.TC022.class,
        tauc.ts0.TC023.class,
        tauc.ts0.TC024.class,
        tauc.ts0.TC025.class,
        tauc.ts0.TC026.class,
        tauc.ts0.TC027.class,
        tauc.ts0.TC028.class,
        tauc.ts0.TC029.class,
        tauc.ts0.TC030.class,
        tauc.ts0.TC031.class,
        tauc.ts0.TC032.class,
        tauc.ts0.TC033.class,
        tauc.ts0.TC034.class,
        tauc.ts0.TC035.class,
        tauc.ts0.TC036.class,
        tauc.ts0.TC037.class,
        tauc.ts0.TC038.class,
        tauc.ts0.TC039.class,
        tauc.ts0.TC040.class,
        tauc.ts0.TC041.class,
        tauc.ts0.TC042.class,
        tauc.ts0.TC043.class,
        tauc.ts0.TC044.class,
        tauc.ts0.TC045.class,
        tauc.ts0.TC046.class,
        tauc.ts0.TC047.class,
        tauc.ts0.TC048.class,
        tauc.ts0.TC049.class,
        tauc.ts0.TC050.class,
        tauc.ts0.TC051.class,
        tauc.ts0.TC052.class,
        tauc.ts0.TC053.class,
        tauc.ts0.TC054.class,
        tauc.ts0.TC055.class,
        tauc.ts0.TC056.class,
        tauc.ts0.TC057.class,
        tauc.ts0.TC058.class,
        tauc.ts0.TC059.class,
        tauc.ts0.TC060.class,
        tauc.ts0.TC061.class,
        tauc.ts0.TC062.class,
        tauc.ts0.TC063.class,
        tauc.ts0.TC064.class,
        tauc.ts0.TC065.class,
        tauc.ts0.TC066.class,
        tauc.ts0.TC067.class,
        tauc.ts0.TC068.class,
        tauc.ts0.TC069.class,
        tauc.ts0.TC070.class,
        tauc.ts0.TC071.class,
        tauc.ts0.TC072.class,
        tauc.ts0.TC073.class,
        tauc.ts0.TC074.class,
        tauc.ts0.TC075.class,
        tauc.ts0.TC076.class,
        tauc.ts0.TC077.class,
        tauc.ts0.TC078.class,
        tauc.ts0.TC079.class,
        tauc.ts0.TC080.class,
        tauc.ts0.TC081.class,
        tauc.ts0.TC082.class,
        tauc.ts0.TC083.class,
        tauc.ts0.TC084.class,
        tauc.ts0.TC085.class,
        tauc.ts0.TC086.class,
        tauc.ts0.TC087.class,
        tauc.ts0.TC088.class,
        tauc.ts0.TC089.class,
        tauc.ts0.TC090.class,
        tauc.ts0.TC091.class,
        tauc.ts0.TC092.class,
        tauc.ts0.TC093.class,
        tauc.ts0.TC094.class,
        tauc.ts0.TC095.class,
        tauc.ts0.TC096.class,
        tauc.ts0.TC097.class,
        tauc.ts0.TC098.class,
        tauc.ts0.TC099.class

})
public class TestSuiteTAUC0 {
}
