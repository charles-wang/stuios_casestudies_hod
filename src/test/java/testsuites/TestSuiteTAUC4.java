package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author chunhui.wang 04/08/2017
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        tauc.ts4.TC000.class,
        tauc.ts4.TC001.class,
        tauc.ts4.TC002.class,
        tauc.ts4.TC003.class,
        tauc.ts4.TC004.class,
        tauc.ts4.TC005.class,
        tauc.ts4.TC006.class,
        tauc.ts4.TC007.class,
        tauc.ts4.TC008.class,
        tauc.ts4.TC009.class,
        tauc.ts4.TC010.class,
        tauc.ts4.TC011.class,
        tauc.ts4.TC012.class,
        tauc.ts4.TC013.class,
        tauc.ts4.TC014.class,
        tauc.ts4.TC015.class,
        tauc.ts4.TC016.class,
        tauc.ts4.TC017.class,
        tauc.ts4.TC018.class,
        tauc.ts4.TC019.class,
        tauc.ts4.TC020.class,
        tauc.ts4.TC021.class,
        tauc.ts4.TC022.class,
        tauc.ts4.TC023.class,
        tauc.ts4.TC024.class,
        tauc.ts4.TC025.class,
        tauc.ts4.TC026.class,
        tauc.ts4.TC027.class,
        tauc.ts4.TC028.class,
        tauc.ts4.TC029.class,
        tauc.ts4.TC030.class,
        tauc.ts4.TC031.class,
        tauc.ts4.TC032.class,
        tauc.ts4.TC033.class,
        tauc.ts4.TC034.class,
        tauc.ts4.TC035.class,
        tauc.ts4.TC036.class,
        tauc.ts4.TC037.class,
        tauc.ts4.TC038.class,
        tauc.ts4.TC039.class,
        tauc.ts4.TC040.class,
        tauc.ts4.TC041.class,
        tauc.ts4.TC042.class,
        tauc.ts4.TC043.class,
        tauc.ts4.TC044.class,
        tauc.ts4.TC045.class,
        tauc.ts4.TC046.class,
        tauc.ts4.TC047.class,
        tauc.ts4.TC048.class,
        tauc.ts4.TC049.class,
        tauc.ts4.TC050.class,
        tauc.ts4.TC051.class,
        tauc.ts4.TC052.class,
        tauc.ts4.TC053.class,
        tauc.ts4.TC054.class,
        tauc.ts4.TC055.class,
        tauc.ts4.TC056.class,
        tauc.ts4.TC057.class,
        tauc.ts4.TC058.class,
        tauc.ts4.TC059.class,
        tauc.ts4.TC060.class,
        tauc.ts4.TC061.class,
        tauc.ts4.TC062.class,
        tauc.ts4.TC063.class,
        tauc.ts4.TC064.class,
        tauc.ts4.TC065.class,
        tauc.ts4.TC066.class,
        tauc.ts4.TC067.class,
        tauc.ts4.TC068.class,
        tauc.ts4.TC069.class,
        tauc.ts4.TC070.class,
        tauc.ts4.TC071.class,
        tauc.ts4.TC072.class,
        tauc.ts4.TC073.class,
        tauc.ts4.TC074.class,
        tauc.ts4.TC075.class,
        tauc.ts4.TC076.class,
        tauc.ts4.TC077.class,
        tauc.ts4.TC078.class,
        tauc.ts4.TC079.class,
        tauc.ts4.TC080.class,
        tauc.ts4.TC081.class,
        tauc.ts4.TC082.class,
        tauc.ts4.TC083.class,
        tauc.ts4.TC084.class,
        tauc.ts4.TC085.class,
        tauc.ts4.TC086.class,
        tauc.ts4.TC087.class,
        tauc.ts4.TC088.class,
        tauc.ts4.TC089.class,
        tauc.ts4.TC090.class,
        tauc.ts4.TC091.class,
        tauc.ts4.TC092.class,
        tauc.ts4.TC093.class,
        tauc.ts4.TC094.class,
        tauc.ts4.TC095.class,
        tauc.ts4.TC096.class,
        tauc.ts4.TC097.class,
        tauc.ts4.TC098.class,
        tauc.ts4.TC099.class

})
public class TestSuiteTAUC4 {
}
