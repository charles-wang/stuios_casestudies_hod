package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author chunhui.wang 04/08/2017
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        stuios.fastqualification.TC000.class,
        stuios.fastqualification.TC001.class,
        stuios.fastqualification.TC002.class,
        stuios.fastqualification.TC003.class,
        stuios.fastqualification.TC004.class,
        stuios.fastqualification.TC005.class,
        stuios.fastqualification.TC006.class,
        stuios.fastqualification.TC007.class,
        stuios.fastqualification.TC008.class,
        stuios.fastqualification.TC009.class,
        stuios.fastqualification.TC010.class,
        stuios.fastqualification.TC011.class,
        stuios.fastqualification.TC012.class,
        stuios.fastqualification.TC013.class,
        stuios.fastqualification.TC014.class,
        stuios.fastqualification.TC015.class,
        stuios.fastqualification.TC016.class,
        stuios.fastqualification.TC017.class,
        stuios.fastqualification.TC018.class,
        stuios.fastqualification.TC019.class,
        stuios.fastqualification.TC020.class,
        stuios.fastqualification.TC021.class,
        stuios.fastqualification.TC022.class,
        stuios.fastqualification.TC023.class,
        stuios.fastqualification.TC024.class,
        stuios.fastqualification.TC025.class,
        stuios.fastqualification.TC026.class,
        stuios.fastqualification.TC027.class,
        stuios.fastqualification.TC028.class,
        stuios.fastqualification.TC029.class,
        stuios.fastqualification.TC030.class,
        stuios.fastqualification.TC031.class,
        stuios.fastqualification.TC032.class

})
public class TestSuiteSTUIOSReduced {
}
