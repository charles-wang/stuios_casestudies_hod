package tauc.ts7;
import testadaptor.BodySenseAdaptor;
public class TC010 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\": \"s[27]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 0"
+ "}");








verify("{"
+ "\"instance\": \"asic_com_error\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\", \"value\":true},"
+ "{\"name\":\"Qualified\",\"value\":true}"
+ "],"
+ "\"timestamp\": 1700"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[30]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 1700"
+ "}");








verify("{"
+ "\"instance\": \"asic_com_error\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\", \"value\":false},"
+ "{\"name\":\"Qualified\",\"value\":false}"
+ "],"
+ "\"timestamp\": 3400"
+ "}");


}
}