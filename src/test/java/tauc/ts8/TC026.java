package tauc.ts8;
import testadaptor.BodySenseAdaptor;
public class TC026 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\": \"s[23]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 0"
+ "}");








verify("{"
+ "\"instance\": \"asic_ak_parity_error\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\", \"value\":true},"
+ "{\"name\":\"Qualified\",\"value\":true}"
+ "],"
+ "\"timestamp\": 1700"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[26]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 1700"
+ "}");








verify("{"
+ "\"instance\": \"asic_ak_parity_error\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\", \"value\":false},"
+ "{\"name\":\"Qualified\",\"value\":false}"
+ "],"
+ "\"timestamp\": 3400"
+ "}");


}
}