package tauc.ts4;
import testadaptor.BodySenseAdaptor;
public class TC097 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\": \"s[59]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 0"
+ "}");




verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":true},"
  + "{\"name\":\"Qualified\",\"value\":false}"
  + "],"
  + "\"timestamp\": 0"
+ "}");



verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":true},"
  + "{\"name\":\"Qualified\",\"value\":true}"
  + "],"
  + "\"timestamp\": 4800"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[62]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 4800"
+ "}");




verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":false},"
  + "{\"name\":\"Qualified\",\"value\":true}"
  + "],"
  + "\"timestamp\": 4800"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[61]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 4800"
+ "}");




verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":true},"
  + "{\"name\":\"Qualified\",\"value\":true}"
  + "],"
  + "\"timestamp\": 4800"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[62]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 4800"
+ "}");




verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":false},"
  + "{\"name\":\"Qualified\",\"value\":true}"
  + "],"
  + "\"timestamp\": 4800"
+ "}");



verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":false},"
  + "{\"name\":\"Qualified\",\"value\":false}"
  + "],"
  + "\"timestamp\": 13000"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[59]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 13000"
+ "}");




verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":true},"
  + "{\"name\":\"Qualified\",\"value\":false}"
  + "],"
  + "\"timestamp\": 13000"
+ "}");



verify("{"
  + "\"instance\": \"algo_real_high_cutoff_error\","
  + "\"variables\": ["
  + "{\"name\":\"Detected\", \"value\":true},"
  + "{\"name\":\"Qualified\",\"value\":true}"
  + "],"
  + "\"timestamp\": 17800"
+ "}");


executeScenario("{"
+ "\"scenario\": \"s[62]\","
+ "\"parameters\": ["
+ "{\"name\":null,\"value\":null}"
+ "],"
+ "\"timestamp\": 17800"
+ "}");



}
}