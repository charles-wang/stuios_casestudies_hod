package stuios.ts5;
import testadaptor.BodySenseAdaptor;
public class TC038 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[47]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[50]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[49]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[50]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[47]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":26000}");
executeScenario("{\"scenario\":\"s[50]\",\"timestamp\":26000}");
verify("{\"instance\":\"capa_ld_curr_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":34200}");

}
}