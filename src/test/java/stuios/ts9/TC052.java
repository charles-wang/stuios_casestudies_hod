package stuios.ts9;
import testadaptor.BodySenseAdaptor;
public class TC052 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[9]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[12]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[9]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
executeScenario("{\"scenario\":\"s[12]\",\"parameters\":[{}],\"timestamp\":17800}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":26000}");
executeScenario("{\"scenario\":\"s[9]\",\"parameters\":[{}],\"timestamp\":26000}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":26000}");
verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":30800}");

}
}