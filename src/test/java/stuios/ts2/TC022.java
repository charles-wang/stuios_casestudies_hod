package stuios.ts2;
import testadaptor.BodySenseAdaptor;
public class TC022 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[83]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[86]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[85]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[86]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[83]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[84]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":17800}");
executeScenario("{\"scenario\":\"s[83]\",\"timestamp\":17800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":22600}");

}
}