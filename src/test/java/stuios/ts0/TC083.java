package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC083 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":1700}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":1700}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":3400}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":3400}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":5100}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":5100}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":6800}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":6800}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":8500}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":8500}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":10200}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":10200}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":11900}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":11900}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":13600}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":13600}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":15300}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":17000}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":17000}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":18700}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":18700}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":20400}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":20400}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":22100}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":22100}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":26900}");
        executeScenario("{\"scenario\":\"s[5]\",\"timestamp\":26900}");
        verify("{\"instance\":\"over_voltage_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":31700}");

    }
}