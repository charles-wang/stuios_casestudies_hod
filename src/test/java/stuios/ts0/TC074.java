package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC074 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[59]\","
                + "\"parameters\":[{}],\"timestamp\":1700}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":1700}");
        executeScenario("{\"scenario\":\"s[59]\","
                + "\"parameters\":[{}],\"timestamp\":3400}");
        executeScenario("{\"scenario\":\"s[59]\","
                + "\"parameters\":[{}],\"timestamp\":5100}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":6500}");
        executeScenario("{\"scenario\":\"s[62]\","
                + "\"parameters\":[{}],\"timestamp\":6800}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":6800}");
        executeScenario("{\"scenario\":\"s[61]\","
                + "\"parameters\":[{}],\"timestamp\":8500}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":8500}");
        executeScenario("{\"scenario\":\"s[62]\","
                + "\"parameters\":[{}],\"timestamp\":10200}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":10200}");
        executeScenario("{\"scenario\":\"s[62]\","
                + "\"parameters\":[{}],\"timestamp\":11900}");
        executeScenario("{\"scenario\":\"s[61]\","
                + "\"parameters\":[{}],\"timestamp\":13600}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":13600}");
        executeScenario("{\"scenario\":\"s[62]\","
                + "\"parameters\":[{}],\"timestamp\":15300}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":15300}");
        executeScenario("{\"scenario\":\"s[61]\","
                + "\"parameters\":[{}],\"timestamp\":17000}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":17000}");
        executeScenario("{\"scenario\":\"s[62]\","
                + "\"parameters\":[{}],\"timestamp\":18700}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":18700}");
        executeScenario("{\"scenario\":\"s[61]\","
                + "\"parameters\":[{}],\"timestamp\":20400}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":20400}");
        executeScenario("{\"scenario\":\"s[61]\","
                + "\"parameters\":[{}],\"timestamp\":22100}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":30300}");
        executeScenario
                ("{\"scenario\":\"s[62]\",\"timestamp\":30300}");
        verify("{\"instance\":\"algo_real_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":38500}");

    }
}