package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC008 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[13]\","
                + "\"parameters\": ["
                + "{\"name\":null,\"value\":null}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"nvm_access_error\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


    }
}