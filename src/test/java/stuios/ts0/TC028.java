package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC028 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[17]\","
                + "\"parameters\": ["
                + "{\"name\":null,\"value\":null}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"calibration_data_error\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


    }
}