package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC043 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[9]\","
                + "\"parameters\":[{}],\"timestamp\":1700}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":1700}");
        executeScenario("{\"scenario\":\"s[9]\","
                + "\"parameters\":[{}],\"timestamp\":3400}");
        executeScenario("{\"scenario\":\"s[9]\","
                + "\"parameters\":[{}],\"timestamp\":5100}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":6500}");
        executeScenario("{\"scenario\":\"s[12]\","
                + "\"parameters\":[{}],\"timestamp\":6800}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":6800}");
        executeScenario("{\"scenario\":\"s[11]\","
                + "\"parameters\":[{}],\"timestamp\":8500}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":8500}");
        executeScenario("{\"scenario\":\"s[12]\","
                + "\"parameters\":[{}],\"timestamp\":10200}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":10200}");
        executeScenario("{\"scenario\":\"s[12]\","
                + "\"parameters\":[{}],\"timestamp\":11900}");
        executeScenario("{\"scenario\":\"s[11]\","
                + "\"parameters\":[{}],\"timestamp\":13600}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":13600}");
        executeScenario("{\"scenario\":\"s[12]\","
                + "\"parameters\":[{}],\"timestamp\":15300}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":15300}");
        executeScenario("{\"scenario\":\"s[11]\","
                + "\"parameters\":[{}],\"timestamp\":17000}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":17000}");
        executeScenario("{\"scenario\":\"s[11]\","
                + "\"parameters\":[{}],\"timestamp\":18700}");
        executeScenario("{\"scenario\":\"s[12]\","
                + "\"parameters\":[{}],\"timestamp\":20400}");
        verify("{\"instance\":\"vbat_out_of_range_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":20400}");
        verify("{\"instance\":\"vbat_out_of_range_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":28600}");

    }
}