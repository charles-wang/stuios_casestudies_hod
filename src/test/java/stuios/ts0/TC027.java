package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC027 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":1700}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":1700}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":3400}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":5100}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":6500}");
        executeScenario("{\"scenario\":\"s[8]\","
                + "\"parameters\":[{}],\"timestamp\":6800}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":6800}");
        executeScenario("{\"scenario\":\"s[7]\","
                + "\"parameters\":[{}],\"timestamp\":8500}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":8500}");
        executeScenario("{\"scenario\":\"s[8]\","
                + "\"parameters\":[{}],\"timestamp\":10200}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}],\"timestamp\":10200}");
        executeScenario("{\"scenario\":\"s[8]\","
                + "\"parameters\":[{}],\"timestamp\":11900}");
        executeScenario("{\"scenario\":\"s[8]\","
                + "\"parameters\":[{}],\"timestamp\":13600}");
        executeScenario("{\"scenario\":\"s[8]\","
                + "\"parameters\":[{}],\"timestamp\":15300}");
        executeScenario("{\"scenario\":\"s[8]\","
                + "\"parameters\":[{}],\"timestamp\":17000}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":18400}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"parameters\":[{}],\"timestamp\":18700}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":18700}");
        executeScenario("{\"scenario\":\"s[6]\","
                + "\"parameters\":[{}],\"timestamp\":20400}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":20400}");
        verify("{\"instance\":\"over_voltage_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":25200}");
        executeScenario("{\"scenario\":\"s[5]\","
                + "\"timestamp\":25200}");
        verify("{\"instance\":\"over_voltage_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":30000}");

    }
}