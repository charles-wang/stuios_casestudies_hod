package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC003 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[16]\","
                + "\"parameters\": ["
                + "{\"name\":null,\"value\":null}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"algo_selection_error\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


    }
}