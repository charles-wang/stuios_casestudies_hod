package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC001 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[75]\","
                + "\"parameters\":[{}],\"timestamp\":1700}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":1700}");
        executeScenario("{\"scenario\":\"s[75]\","
                + "\"parameters\":[{}],\"timestamp\":3400}");
        executeScenario("{\"scenario\":\"s[75]\","
                + "\"parameters\":[{}],\"timestamp\":5100}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":6500}");
        executeScenario("{\"scenario\":\"s[78]\","
                + "\"parameters\":[{}],\"timestamp\":6800}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":6800}");
        executeScenario("{\"scenario\":\"s[77]\","
                + "\"parameters\":[{}],\"timestamp\":8500}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":8500}");
        executeScenario("{\"scenario\":\"s[78]\","
                + "\"parameters\":[{}],\"timestamp\":10200}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":10200}");
        executeScenario("{\"scenario\":\"s[78]\","
                + "\"parameters\":[{}],\"timestamp\":11900}");
        executeScenario("{\"scenario\":\"s[78]\","
                + "\"parameters\":[{}],\"timestamp\":13600}");
        executeScenario("{\"scenario\":\"s[78]\","
                + "\"parameters\":[{}],\"timestamp\":15300}");
        executeScenario("{\"scenario\":\"s[78]\","
                + "\"parameters\":[{}],\"timestamp\":17000}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":18400}");
        executeScenario("{\"scenario\":\"s[75]\","
                + "\"parameters\":[{}],\"timestamp\":18700}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":18700}");
        executeScenario("{\"scenario\":\"s[76]\","
                + "\"parameters\":[{}],\"timestamp\":20400}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":20400}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":25200}");
        executeScenario("{\"scenario\":\"s[75]\","
                + "\"timestamp\":25200}");
        verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":30000}");

    }
}