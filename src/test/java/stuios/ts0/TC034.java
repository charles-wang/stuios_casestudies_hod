package stuios.ts0;

import testadaptor.BodySenseAdaptor;

public class TC034 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":1700}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":1700}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":3400}");
        executeScenario("{\"scenario\":\"s[96]\","
                + "\"parameters\":[{}],\"timestamp\":5100}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":5100}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":6800}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":6800}");
        executeScenario("{\"scenario\":\"s[96]\","
                + "\"parameters\":[{}],\"timestamp\":8500}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":8500}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":10200}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":10200}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":11900}");
        executeScenario("{\"scenario\":\"s[96]\","
                + "\"parameters\":[{}],\"timestamp\":13600}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":13600}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":15300}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":15300}");
        executeScenario("{\"scenario\":\"s[96]\","
                + "\"parameters\":[{}],\"timestamp\":17000}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":17000}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":18700}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":18700}");
        executeScenario("{\"scenario\":\"s[96]\","
                + "\"parameters\":[{}],\"timestamp\":20400}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":20400}");
        executeScenario("{\"scenario\":\"s[95]\","
                + "\"parameters\":[{}],\"timestamp\":22100}");
        verify("{\"instance\":\"guard_diag_error\","
                + "\"variables\":[{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}],\"timestamp\":22100}");
        verify("{\"instance\":\"guard_diag_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":26900}");

    }
}