package stuios.ts7;
import testadaptor.BodySenseAdaptor;
public class TC042 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[63]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[66]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[65]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[66]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[63]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":26000}");
executeScenario("{\"scenario\":\"s[66]\",\"timestamp\":26000}");
verify("{\"instance\":\"algo_imag_low_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":34200}");

}
}