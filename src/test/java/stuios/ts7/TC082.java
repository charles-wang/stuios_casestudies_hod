package stuios.ts7;
import testadaptor.BodySenseAdaptor;
public class TC082 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[31]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[34]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[33]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[34]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[31]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":26000}");
executeScenario("{\"scenario\":\"s[34]\",\"timestamp\":26000}");
verify("{\"instance\":\"capa_offset_real_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":34200}");

}
}