package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC002 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[35]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"capa_offset_real_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4250}");
        verify("{\"instance\":\"capa_offset_real_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":16400}");
        executeScenario("{\"scenario\":\"s[38]\",\"timestamp\":16400}");
        verify("{\"instance\":\"capa_offset_real_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":24900}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}