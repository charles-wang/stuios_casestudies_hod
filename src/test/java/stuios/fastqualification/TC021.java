package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC021 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[91]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"asic_comp_test_c404142_electrical_device_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4250}");
        verify("{\"instance\":\"asic_comp_test_c404142_electrical_device_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":16400}");
        executeScenario("{\"scenario\":\"s[94]\",\"timestamp\":16400}");
        verify("{\"instance\":\"asic_comp_test_c404142_electrical_device_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":24900}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}