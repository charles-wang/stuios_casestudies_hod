package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC023 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[19]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"ram_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":850}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}