package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC026 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[23]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"asic_ak_parity_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":850}");
        executeScenario("{\"scenario\":\"s[26]\",\"timestamp\":850}");
        verify("{\"instance\":\"asic_ak_parity_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":2550}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}