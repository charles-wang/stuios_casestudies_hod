package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC030 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[20]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"alu_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":850}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}