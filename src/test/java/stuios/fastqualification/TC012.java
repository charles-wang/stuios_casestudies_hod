package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC012 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        executeScenario("{\"scenario\":\"s[87]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4250}");
        verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":16400}");
        executeScenario("{\"scenario\":\"s[90]\",\"timestamp\":16400}");
        verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":24900}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}