package stuios.fastqualification;

import testadaptor.BodySenseAdaptor;

public class TC016 extends BodySenseAdaptor {
    @Override
    public void generatedTest() {
        verify("{\"instance\":\"watchdog_reset_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
        executeScenario("{\"scenario\":\"s[21]\",\"parameters\":[{}],\"timestamp\":0}");
        verify("{\"instance\":\"watchdog_reset_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":850}");

    }

    @Override
    public double getProbability() {
        return 0.5;
    }
}