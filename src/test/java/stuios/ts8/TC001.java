package stuios.ts8;
import testadaptor.BodySenseAdaptor;
public class TC001 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[75]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[78]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[77]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[78]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[75]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[76]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":17800}");
executeScenario("{\"scenario\":\"s[75]\",\"timestamp\":17800}");
verify("{\"instance\":\"asic_comp_test_c404142_vch_too_low_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":22600}");

}
}