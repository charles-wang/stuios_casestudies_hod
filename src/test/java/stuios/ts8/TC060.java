package stuios.ts8;
import testadaptor.BodySenseAdaptor;
public class TC060 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[87]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[90]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[87]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
executeScenario("{\"scenario\":\"s[90]\",\"parameters\":[{}],\"timestamp\":17800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":26000}");
executeScenario("{\"scenario\":\"s[87]\",\"parameters\":[{}],\"timestamp\":26000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":26000}");
verify("{\"instance\":\"asic_comp_test_cref_vch_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":30800}");

}
}