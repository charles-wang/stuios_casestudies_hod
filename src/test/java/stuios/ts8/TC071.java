package stuios.ts8;
import testadaptor.BodySenseAdaptor;
public class TC071 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[43]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[46]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[44]\",\"parameters\":[{}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[43]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
executeScenario("{\"scenario\":\"s[46]\",\"parameters\":[{}],\"timestamp\":17800}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":17800}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":26000}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":30800}");
executeScenario("{\"scenario\":\"s[43]\",\"timestamp\":30800}");
verify("{\"instance\":\"capa_offset_imag_too_high_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":35600}");

}
}