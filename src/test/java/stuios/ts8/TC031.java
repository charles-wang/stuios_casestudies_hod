package stuios.ts8;
import testadaptor.BodySenseAdaptor;
public class TC031 extends BodySenseAdaptor{
    @Override
    public void generatedTest() {
executeScenario("{\"scenario\":\"s[67]\",\"parameters\":[{}],\"timestamp\":0}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":0}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[70]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[69]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
executeScenario("{\"scenario\":\"s[70]\",\"parameters\":[{}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":4800}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[67]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
executeScenario("{\"scenario\":\"s[68]\",\"parameters\":[{}],\"timestamp\":13000}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":13000}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"false\"},{\"name\":\"Qualified\",\"value\":\"false\"}],\"timestamp\":17800}");
executeScenario("{\"scenario\":\"s[67]\",\"timestamp\":17800}");
verify("{\"instance\":\"algo_imag_high_cutoff_error\",\"variables\":[{\"name\":\"Detected\",\"value\":\"true\"},{\"name\":\"Qualified\",\"value\":\"true\"}],\"timestamp\":22600}");

}
}